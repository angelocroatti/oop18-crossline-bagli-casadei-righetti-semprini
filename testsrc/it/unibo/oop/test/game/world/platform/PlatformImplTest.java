package it.unibo.oop.test.game.world.platform;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.world.platform.PlatformImpl;

/**
 * Test class for PlatformImpl.
 */
public class PlatformImplTest {

    /**
     * Zero value.
     */
    private static final int ZERO = 0;
    /**
     * First platform.
     */
    private PlatformImpl platform1;
    /**
     * Second platform.
     */
    private PlatformImpl platform2;
    /**
     * Same as first platform.
     */
    private PlatformImpl sameAsPlatform1;
    /**
     * Unimplemented platform.
     */
    private static final PlatformImpl UNIMPLEMENTED_PLATFORM = null;
    private static final float EARTH_GRAVITY = -9.8f;

    /**
     * SetUp method for instance platforms.
     * 
     * @throws java.lang.Exception generic exception
     */
    @Before
    public void setUp() throws Exception {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        platform1 = new PlatformImpl(world, new Vector2(1, 0));
        platform2 = new PlatformImpl(world, new Vector2(0, 1));
        sameAsPlatform1 = platform1;
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.platform.PlatformImpl#hashCode()}.
     */
    @Test
    public final void testHashCode() {
        assertNotEquals(platform1.hashCode(), ZERO);
        assertNotEquals(platform2.hashCode(), ZERO);
        assertNotEquals(platform1.hashCode(), platform2.hashCode());
        assertEquals("platform1 equals to sameAsPlatform1", platform1.hashCode(), sameAsPlatform1.hashCode());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.platform.PlatformImpl#PlatformImpl(com.badlogic.gdx.physics.box2d.World, com.badlogic.gdx.math.Vector2)}.
     */
    @Test
    public final void testPlatformImpl() {
        assertNotNull("platform1 is not null", platform1);
        assertNotNull("platform2 is not null", platform2);
        assertNotNull("sameAsPlatform1 is not null", sameAsPlatform1);
        assertNull("UNIMPLEMENTED_PLATFORM is null", UNIMPLEMENTED_PLATFORM);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.platform.PlatformImpl#getBody()}.
     */
    @Test
    public final void testGetBody() {
        assertNotNull("the body of platform1 is not null", platform1.getBody());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.platform.PlatformImpl#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        assertNotEquals(platform1, platform2);
        assertEquals("platform1 equals to sameAsPlatform1", platform1, sameAsPlatform1);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.platform.PlatformImpl#toString()}.
     */
    @Test
    public final void testToString() {
        assertNotNull("the string of platform1 is not null", platform1.toString());
    }

}
