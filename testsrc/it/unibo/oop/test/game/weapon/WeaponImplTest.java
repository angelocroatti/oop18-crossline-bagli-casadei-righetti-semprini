package it.unibo.oop.test.game.weapon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.actor.player.PlayerImpl;
import it.unibo.oop.crossline.game.bullet.BulletBuilder;
import it.unibo.oop.crossline.game.bullet.BulletBuilderImpl;
import it.unibo.oop.crossline.game.weapon.WeaponImpl;

/**
 * Test class for WaponImpl.
 */
public class WeaponImplTest {

    private static final float BASE_BULLET_DAMAGE = 10f;
    private static final float BASE_BULLET_SPEED = 2f;
    private static final long SHOT_DELAY1 = 3000;
    private static final long SHOT_DELAY2 = 2000;
    private static final float DIFFICULTY = 5;
    private static final int ZERO = 0;
    private static final float EARTH_GRAVITY = -9.8f;

    /**
     * First weapon.
     */
    private WeaponImpl weapon1;
    /**
     * Second weapon.
     */
    private WeaponImpl weapon2;
    /**
     * Same as first weapon.
     */
    private WeaponImpl sameAsWeapon1;
    private PlayerImpl player;
    /**
     * Unimplemented weapon.
     */
    private static final WeaponImpl UNIMPLEMENTED_WEAPON = null;

    /**
     * SetUp method for instance weapons.
     * 
     * @throws java.lang.Exception generic exception
     */
    @Before
    public void setUp() throws Exception {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        player = new PlayerImpl(world, new Vector2(ZERO, ZERO));
        final BulletBuilder bulletBuilder = new BulletBuilderImpl().setDamage(BASE_BULLET_DAMAGE * DIFFICULTY)
                .setSpeed(BASE_BULLET_SPEED * DIFFICULTY);
        weapon1 = new WeaponImpl(SHOT_DELAY1, bulletBuilder);
        weapon2 = new WeaponImpl(SHOT_DELAY2, bulletBuilder);
        sameAsWeapon1 = weapon1;
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#hashCode()}.
     */
    @Test
    public void testHashCode() {
        assertNotEquals(weapon1.hashCode(), ZERO);
        assertNotEquals(weapon2.hashCode(), ZERO);
        assertNotEquals(weapon1.hashCode(), weapon2.hashCode());
        assertEquals("weapon1 equals to sameAsWeapon1", weapon1.hashCode(), sameAsWeapon1.hashCode());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#WeaponImpl(long, it.unibo.oop.crossline.game.bullet.BulletBuilder)}.
     */
    @Test
    public final void testWeaponImpl() {
        assertNotNull("weapon1 is not null", weapon1);
        assertNotNull("weapon2 is not null", weapon2);
        assertNotNull("sameAsWeapon1 is not null", sameAsWeapon1);
        assertNull("UNIMPLEMENTED_WEAPON is null", UNIMPLEMENTED_WEAPON);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#canShoot()}.
     */
    @Test
    public final void testCanShoot() {
        assertFalse("weapon1 can't shoot as well", weapon1.canShoot());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#getBulletBuilder()}.
     */
    @Test
    public final void testGetBulletFactory() {
        assertNotNull("weapon1 has bullet factory", weapon1.getBulletBuilder());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#getDirection()}.
     */
    @Test
    public final void testGetDirection() {
        assertNotNull("weapon1 has direction", weapon1.getDirection());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#setDirection(com.badlogic.gdx.math.Vector2)}.
     */
    @Test
    public final void testSetDirection() {
        final Vector2 direction = new Vector2(1, 0);
        weapon1.setDirection(direction);
        assertEquals("weapon1's direction equals to direction", weapon1.getDirection(), direction);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#setOwner(it.unibo.oop.crossline.game.actor.Actor)}.
     */
    @Test
    public final void testSetOwner() {
        weapon1.setOwner(player);
        assertTrue("weapon1's owner equals to player", weapon1.toString().contains(player.toString()));
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#shoot()}.
     * 
     * @throws InterruptedException for threading sleep
     */
    @Test
    public final void testShoot() throws InterruptedException {
        weapon1.setOwner(player);
        final String before = weapon1.toString();
        weapon1.shoot();
        // without wait, maybe the time between one shoot and other could be zero,
        // making fail the test
        Thread.sleep(1);
        weapon1.shoot();
        final String after = weapon1.toString();
        assertNotEquals("weapon1 has shooted well", before, after);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#toString()}.
     */
    @Test
    public void testToString() {
        assertNotNull("the string of weapon1 is not null", weapon1.toString());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.weapon.WeaponImpl#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObject() {
        weapon1.setOwner(player);
        weapon2.setOwner(player);
        assertNotEquals(weapon1, weapon2);
        assertEquals("weapon1 equals to sameAsWeapon1", weapon1, sameAsWeapon1);
    }
}
