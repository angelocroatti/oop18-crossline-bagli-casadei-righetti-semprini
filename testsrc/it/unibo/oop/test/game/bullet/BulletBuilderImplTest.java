package it.unibo.oop.test.game.bullet;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.actor.Actor;
import it.unibo.oop.crossline.game.actor.player.PlayerImpl;
import it.unibo.oop.crossline.game.bullet.BulletBuilderImpl;
import it.unibo.oop.crossline.game.bullet.BulletImpl;

/**
 * Test class for BulletBuilderImpl.
 */
public class BulletBuilderImplTest {

    private static final int ZERO = 0;
    private static final Vector2 NULL_VECTOR = new Vector2(ZERO, ZERO);
    private static final World WORLD = new World(NULL_VECTOR, true);
    private static final Actor OWNER = new PlayerImpl(WORLD, NULL_VECTOR);

    /**
     * Test the bulletBuilder correct usage.
     */
    @Test
    public final void test() {
        final BulletBuilderImpl builder = new BulletBuilderImpl();
        final BulletImpl bullet1 = new BulletImpl(ZERO, NULL_VECTOR, OWNER, NULL_VECTOR, ZERO, ZERO);
        final BulletImpl bullet2 = (BulletImpl) builder
                .setDamage(ZERO)
                .setDirection(NULL_VECTOR)
                .setOwner(OWNER)
                .setPosition(NULL_VECTOR)
                .setSpeed(ZERO)
                .build();
        assertEquals("bulletBuilder has created a bullet with damage equals to bullet1's damage", 
                Double.valueOf(bullet2.getDamage()), Double.valueOf(bullet1.getDamage())); 
        assertEquals("bulletBuilder has created a bullet with direction equals to bullet1's direction", 
                bullet2.getDirection(), bullet1.getDirection()); 
        assertEquals("bulletBuilder has created a bullet with owner equals to bullet1's owner", 
                bullet2.getOwner(), bullet1.getOwner()); 
        assertEquals("bulletBuilder has created a bullet with position equals to bullet1's position", 
                bullet2.getPosition(), bullet1.getPosition()); 
        assertEquals("bulletBuilder has created a bullet with speed equals to bullet1's speed", 
                Double.valueOf(bullet2.getSpeed()), Double.valueOf(bullet1.getSpeed())); 
    }

}
