package it.unibo.oop.test.game.actor.robot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.actor.player.PlayerImpl;
import it.unibo.oop.crossline.game.actor.robot.RobotImpl;
import it.unibo.oop.crossline.game.bullet.BulletBuilder;
import it.unibo.oop.crossline.game.bullet.BulletBuilderImpl;
import it.unibo.oop.crossline.game.wave.WaveImpl;
import it.unibo.oop.crossline.game.weapon.WeaponImpl;

/**
 * Test class for RobotImpl.
 */
public class RobotImplTest {

    /**
     * Zero value.
     */
    private static final int ZERO = 0;
    /**
     * One value.
     */
    private static final int ONE = 1;
    /**
     * First robot.
     */
    private RobotImpl robot1;
    /**
     * Second robot.
     */
    private RobotImpl robot2;
    /**
     * Same as first robot.
     */
    private RobotImpl sameAsRobot1;
    /**
     * Unimplemented robot.
     */
    private static final RobotImpl UNIMPLEMENTED_ROBOT = null;
    private static final double HEALTH_1 = 50.0f;
    private static final double HEALTH_2 = 100.0f;
    private static final float DIFFICULTY = 5;
    private static final float BASE_BULLET_DAMAGE = 10f;
    private static final float BASE_BULLET_SPEED = 2f;
    private static final long SHOT_DELAY1 = 3000;
    private static final long SHOT_DELAY2 = 2000;
    private static final float EARTH_GRAVITY = -9.8f;

    /**
     * SetUp method for instance robots.
     * 
     * @throws java.lang.Exception generic exception
     */
    @Before
    public void setUp() throws Exception {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        final BulletBuilder bulletBuilder = new BulletBuilderImpl().setDamage(BASE_BULLET_DAMAGE * DIFFICULTY)
                .setSpeed(BASE_BULLET_SPEED * DIFFICULTY);
        final WeaponImpl weapon1 = new WeaponImpl(SHOT_DELAY1, bulletBuilder);
        final WeaponImpl weapon2 = new WeaponImpl(SHOT_DELAY2, bulletBuilder);
        robot1 = new RobotImpl(ZERO, (float) HEALTH_1, new Vector2(1, 0), UNIMPLEMENTED_ROBOT, ZERO, weapon1, world);
        robot2 = new RobotImpl(10, (float) HEALTH_2, new Vector2(0, 1), robot1, ONE, weapon2, world);
        sameAsRobot1 = robot1;
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#hashCode()}.
     */
    @Test
    public final void testHashCode() {
        assertNotEquals(robot1.hashCode(), ZERO);
        assertNotEquals(robot2.hashCode(), ZERO);
        assertNotEquals(robot1.hashCode(), robot2.hashCode());
        assertEquals("robot1 equals to sameAsButton1", robot1.hashCode(), sameAsRobot1.hashCode());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#RobotImpl(float, float, com.badlogic.gdx.math.Vector2, it.unibo.oop.crossline.game.attributes.Physical, int, it.unibo.oop.crossline.game.weapon.Weapon, com.badlogic.gdx.physics.box2d.World)}.
     */
    @Test
    public final void testRobotImpl() {
        assertNotNull("robot1 is not null", robot1);
        assertNotNull("robot2 is not null", robot2);
        assertNotNull("sameAsButton1 is not null", sameAsRobot1);
        assertNull("UNIMPLEMENTED_ROBOT is null", UNIMPLEMENTED_ROBOT);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#getBody()}.
     */
    @Test
    public final void testGetBody() {
        assertNotNull("robot1's body is not null", robot1.getBody());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#isQueuedForDestruction()}.
     */
    @Test
    public final void testIsQueuedForDestruction() {
        assertFalse("robot1 is not queued for descruction", robot1.isQueuedForDestruction());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#queueForDestruction()}.
     */
    @Test
    public final void testQueueForDestruction() {
        robot1.queueForDestruction();
        assertTrue("robot1 is queued for descruction", robot1.isQueuedForDestruction());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#getHealth()}.
     */
    @Test
    public final void testGetHealth() {
        assertEquals("robot1's health equals to health", Double.valueOf(robot1.getHealth()), Double.valueOf(HEALTH_1));
        assertEquals("robot2's health equals to health", Double.valueOf(robot2.getHealth()), Double.valueOf(HEALTH_2));
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#applyDamage(float)}.
     */
    @Test
    public final void testApplyDamage() {
        final float damage = 100;
        robot1.applyDamage(damage);
        assertTrue("robot1's life dropped after attack", robot1.isQueuedForDestruction());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#addObserver(java.util.Observer)}.
     */
    @Test
    public final void testAddObserverObserver() {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        final PlayerImpl player1 = new PlayerImpl(world, new Vector2(ZERO, ZERO));
        final PlayerImpl player2 = new PlayerImpl(world, new Vector2(1, 0));
        final PlayerImpl player3 = new PlayerImpl(world, new Vector2(0, 1));
        final Observer observer1 = new WaveImpl(player1, 0);
        final Observer observer2 = new WaveImpl(player2, 1);
        final Observer observer3 = new WaveImpl(player3, 2);
        final List<Observer> observers = new ArrayList<>();
        observers.add(observer1);
        observers.add(observer2);
        observers.add(observer3);
        robot1.addObserver(observer1);
        robot1.addObserver(observer2);
        robot1.addObserver(observer3);
        assertEquals("robot1's obsevers added well", robot1.countObservers(), observers.size());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#getTarget()}.
     */
    @Test
    public final void testGetTarget() {
        assertNotNull("robot1's target is not null", robot1.getWeapon());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#getTeam()}.
     */
    @Test
    public final void testGetTeam() {
        assertNotNull("robot1's team is not null", robot1.getTeam());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#getWeapon()}.
     */
    @Test
    public final void testGetWeapon() {
        assertNotNull("robot1's weapon is not null", robot1.getWeapon());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        assertNotEquals(robot1, robot2);
        assertEquals("robot1 equals to sameAsRobot1", robot1, sameAsRobot1);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.actor.robot.RobotImpl#toString()}.
     */
    @Test
    public final void testToString() {
        assertNotNull("robot1's string is not null", robot1.toString());
    }

}
