package it.unibo.oop.test.game.actor.robot;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.actor.robot.RobotBuilderImpl;
import it.unibo.oop.crossline.game.actor.robot.RobotImpl;
import it.unibo.oop.crossline.game.bullet.BulletBuilder;
import it.unibo.oop.crossline.game.bullet.BulletBuilderImpl;
import it.unibo.oop.crossline.game.weapon.WeaponImpl;

/**
 * Test class for RobotBuilderImpl.
 */
public class RobotBuilderImplTest {

    /**
     * Unimplemented robot.
     */
    private static final RobotImpl UNIMPLEMENTED_ROBOT = null;

    private static final double HEALTH = 50.0f;
    private static final float DIFFICULTY = 5;
    private static final float BASE_BULLET_DAMAGE = 10f;
    private static final float BASE_BULLET_SPEED = 2f;
    private static final long SHOT_DELAY = 3000;
    private static final float EARTH_GRAVITY = -9.8f;
    private static final int ZERO = 0;

    /**
     * Test the robotBuilder correct usage.
     */
    @Test
    public final void test() {
        final RobotBuilderImpl builder = new RobotBuilderImpl();
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        final BulletBuilder bulletBuilder = new BulletBuilderImpl().setDamage(BASE_BULLET_DAMAGE * DIFFICULTY)
                .setSpeed(BASE_BULLET_SPEED * DIFFICULTY);
        final WeaponImpl weapon = new WeaponImpl(SHOT_DELAY, bulletBuilder);
        final RobotImpl robot0 = new RobotImpl(ZERO, (float) HEALTH, new Vector2(0, 1), UNIMPLEMENTED_ROBOT, ZERO, weapon, world);
        final RobotImpl robot1 = new RobotImpl(ZERO, (float) HEALTH, new Vector2(1, 0), robot0, ZERO, weapon, world);
        final RobotImpl robot2 = (RobotImpl) builder
                .setAttackRange(ZERO)
                .setHealth((float) HEALTH)
                .setPosition(new Vector2(1, 0))
                .setTarget(robot0)
                .setTeam(ZERO)
                .setWeapon(weapon)
                .setWorld(world)
                .build();

        assertEquals("robotBuilder has created a robot with health equals to robot1's health", 
                Double.valueOf(robot2.getHealth()), Double.valueOf(robot1.getHealth())); 
        assertEquals("robotBuilder has created a robot with position equals to robot1's position", 
                robot2.getPosition(), robot1.getPosition()); 
        assertEquals("robotBuilder has created a robot with target equals to robot1's target", 
                robot2.getTarget(), robot1.getTarget()); 
        assertEquals("robotBuilder has created a robot with weapon equals to robot1's weapon", 
                robot2.getWeapon(), robot1.getWeapon()); 
        assertEquals("robotBuilder has created a robot with team equals to robot1's team", 
                robot2.getTeam(), robot1.getTeam()); 
    }

}
