package it.unibo.oop.crossline.io.keyboard;

/**
 * Observable interface for keyboard input handle.
 */
public interface KeyboardObservable extends KeyboardObserver {
    /**
     * Add observer to observers list.
     * 
     * @param observer the observer
     * @return true if all is Ok, false when something is gone wrong
     */
    boolean registerObserver(KeyboardObserver observer);

    /**
     * Notify all observers in observers list.
     * 
     * @return true if all is Ok, false when something is gone wrong
     */
    boolean notifyObserver();

    /**
     * Remove observer to observers list.
     * 
     * @param observer the observer
     * @return true if all is Ok, false when something is gone wrong
     */
    boolean removeObserver(KeyboardObserver observer);

}
