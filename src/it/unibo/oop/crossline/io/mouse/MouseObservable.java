package it.unibo.oop.crossline.io.mouse;

import it.unibo.oop.crossline.io.mouse.MouseHandler.MODE;

/**
 * Observable interface for keyboard input handle.
 */
public interface MouseObservable extends MouseObserver {

    /**
     * Add observer to observers list.
     * 
     * @param observer the observer
     * @return true if all is Ok, false when something is gone wrong
     */
    boolean registerObserver(MouseObserver observer);

    /**
     * Notify all observers in observers list.
     * 
     * @param mode the mode we want to use
     * @return true if all is Ok, false when something is gone wrong
     */
    boolean notifyObserver(MODE mode);

    /**
     * Remove observer to observers list.
     * 
     * @param observer the observer
     * @return true if all is Ok, false when something is gone wrong
     */
    boolean removeObserver(MouseObserver observer);

}
