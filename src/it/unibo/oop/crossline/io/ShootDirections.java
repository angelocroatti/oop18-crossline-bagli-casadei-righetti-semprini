package it.unibo.oop.crossline.io;

import java.util.Arrays;
import java.util.stream.Collectors;

import com.badlogic.gdx.math.Vector2;

/**
 * Shooting directions.
 */
public enum ShootDirections {
    /**
     * Up.
     */
    ARR_UP {
        @Override
        public Vector2 getDirection() {
            return new Vector2(0, 1);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Left.
     */
    ARR_LEFT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(-1, 0);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Down.
     */
    ARR_DOWN {
        @Override
        public Vector2 getDirection() {
            return new Vector2(0, -1);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Right.
     */
    ARR_RIGHT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(1, 0);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Up left.
     */
    ARR_UP_LEFT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(-1, 1);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Up right.
     */
    ARR_UP_RIGHT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(1, 1);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Down left.
     */
    ARR_DOWN_LEFT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(-1, -1);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Down right.
     */
    ARR_DOWN_RIGHT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(1, -1);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * XY.
     */
    ARR_XY {
        @Override
        public Vector2 getDirection() {
            return new Vector2(0, 0);
        }

        @Override
        public Vector2 setDirection(final float x, final float y) {
            return new Vector2(x, y);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    };

    /**
     * @return the direction of shoot vector.
     */
    public abstract Vector2 getDirection();

    /**
     * Set the direction of shoot vector.
     * 
     * @param x the x coordinate
     * @param y the y coordinate
     * @return the direction of shoot vector
     */
    public abstract Vector2 setDirection(float x, float y);

    /**
     * Print name of action.
     */
    public abstract void print();

    /**
     * Print all shoot directions.
     */
    public static void printAll() {
        System.out.println("Shoot Directions [");
        Arrays.stream(values()).forEach(System.out::println);
        System.out.println("]");
    }

    /**
     * convertToString method for each direction.
     * 
     * @param name the name
     * @param vect the vector
     * @return a string based on name and vector
     */
    private static String convertToString(final String name, final Vector2 vect) {
        return "Shoot direction [" + "name=" + name + ", direction=" + vect.toString() + "]";
    }

    /**
     * AsString method (like toString one, but for enums).
     * 
     * @return the string of all ements of enum
     */
    public static String asString() {
        return "Shoot directions "
                + Arrays.stream(values()).map(d -> "\n" + d.toString()).collect(Collectors.toList()).toString();
    }
}
