package it.unibo.oop.crossline.game.screen;

import java.awt.Dimension;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import it.unibo.oop.crossline.game.GameControllerImpl;
import it.unibo.oop.crossline.game.GameModelImpl;
import it.unibo.oop.crossline.game.GameViewImpl;

/**
 * Initial screen with background and label.
 */
public class InitialScreen implements Screen {

    private Texture backgroundTexture;
    private SpriteBatch spriteBatch;

    private Stage stage;
    private static final float DISTANCE = 20f;



    @Override
    public final void show() {

        Viewport viewport;
        backgroundTexture = new Texture(Gdx.files.internal("background1920x1080.png"));
        new Sprite(backgroundTexture);
        spriteBatch = new SpriteBatch();
        final Dimension dimension = new Dimension();
        dimension.height = Gdx.graphics.getHeight();
        dimension.width = Gdx.graphics.getWidth();
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                new OrthographicCamera());
        stage = new Stage(viewport, spriteBatch);

        final Label.LabelStyle font = new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("quantum.fnt")),
                Color.WHITE);

        final Table table = new Table();
        table.center();
        table.setFillParent(true);


        final Label labelEnter = new Label("Press Enter to START GAME", font);
        final Label labelMove = new Label("Use keys A D to move player", font);
        final Label labelShoot = new Label("Press Arrow keys to shoot", font);
        final Label labelJump = new Label("Press Space key to jump", font);

        table.add(labelEnter).expandX().padTop(DISTANCE);
        table.row();
        table.add(labelMove).expandX().padTop(DISTANCE);
        table.row();
        table.add(labelShoot).expandX();
        table.row();
        table.add(labelJump).expandX();
        table.row();
        stage.addActor(table);

    }

    @Override
    public final void render(final float arg0) {

        if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
            final Game game = (Game) Gdx.app.getApplicationListener();
            final GameControllerImpl gameController = new GameControllerImpl(new GameViewImpl(),
                    new GameModelImpl());
            this.dispose();
            game.setScreen(gameController);
        }

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.begin();
        //My collaborator don't like this image.
//        /*
//         * draw background
//         */
//        spriteBatch.draw(backgroundSprite, 0, 0,
//                (float) dimension.width,
//                (float) dimension.height);


        spriteBatch.end();
        stage.draw();


    }

    @Override
    public final void dispose() {
        backgroundTexture.dispose();

        stage.dispose();
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resize(final int width, final int height) {
    }

    @Override
    public void resume() {
    }


}
