package it.unibo.oop.crossline.game.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Display GameOver screen and credits.
 */
public class FinalScreen implements Screen {

    private final Stage stage;
    private static final float DISTANCE = 30f;

/**
 * Initialize Screen.
 */
    public FinalScreen() {


        final Viewport viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), new OrthographicCamera());
        stage = new Stage(viewport, new SpriteBatch());

        final Label.LabelStyle font = new Label.LabelStyle(new BitmapFont(Gdx.files.internal("quantum.fnt")), Color.WHITE);

        final Table table = new Table();
        table.center();
        table.setFillParent(true);

        final Label gameOverLabel = new Label("GAME OVER", font);
        final Label labelExit = new Label("Press Enter to EXIT", font);


        table.add(gameOverLabel).expandX();
        table.row();
        table.add(labelExit).expandX().padTop(DISTANCE);
        stage.addActor(table);
    }

    @Override
    public void show() {

    }

    @Override
    public final void render(final float delta) {

        if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
            final Game game = (Game) Gdx.app.getApplicationListener();
            game.dispose();
            this.dispose();
            Gdx.app.exit();
        }

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public final void dispose() {
        stage.dispose();
    }

    @Override
    public void hide() {


    }

    @Override
    public void pause() {


    }

    @Override
    public void resize(final int arg0, final int arg1) {


    }

    @Override
    public void resume() {


    }

}
