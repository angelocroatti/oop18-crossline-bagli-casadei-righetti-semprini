package it.unibo.oop.crossline.game.actor.player;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.weapon.Weapon;

/**
 * This class implements the player interface.
 */
public class PlayerImpl implements Player {

    private static final float MAX_VELOCITY = 4f;
    private static final float MAX_JUMP = 10;
    private static final float CIRCLE_RADIUS = 0.3f;
    private static final float FRICTION = 15;
    private float health = 1;
    private boolean isCurrentlyJumping;
    private boolean shouldShoot;
    private final Body player;
    private boolean queuedForDestruction;

    private Weapon weapon;

    /**
     * @return circle radius
     */
    public static float getCircleRadius() {
        return CIRCLE_RADIUS;

    }

    /**
     * Constructor for player.
     *
     * @param world    the world
     * @param position the position
     */
    public PlayerImpl(final World world, final Vector2 position) {
        final BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(position);
        player = world.createBody(bodyDef);
        player.setUserData(this);

        final Shape circle = new CircleShape();
        circle.setRadius(CIRCLE_RADIUS);

        final FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.friction = FRICTION;
        fixtureDef.shape = circle;

        player.createFixture(fixtureDef);
        circle.dispose();
    }

    @Override
    public final void applyDamage(final float damage) {
        health = Math.max(health - damage, 0);
        if (health == 0) {
            queueForDestruction();
        }
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlayerImpl other = (PlayerImpl) obj;
        if (Float.floatToIntBits(health) != Float.floatToIntBits(other.health)) {
            return false;
        }
        if (isCurrentlyJumping != other.isCurrentlyJumping) {
            return false;
        }
        if (player == null) {
            if (other.player != null) {
                return false;
            }
        } else if (!player.equals(other.player)) {
            return false;
        }
        if (queuedForDestruction != other.queuedForDestruction) {
            return false;
        }
        if (shouldShoot != other.shouldShoot) {
            return false;
        }
        if (weapon == null) {
            if (other.weapon != null) {
                return false;
            }
        } else if (!weapon.equals(other.weapon)) {
            return false;
        }
        return true;
    }

    @Override
    public final Body getBody() {
        return player;
    }

    @Override
    public final float getHealth() {
        return health;
    }

    @Override
    public final boolean isJumping() {
        return this.isCurrentlyJumping;
    }

    @Override
    public final int getTeam() {
        return 0;
    }

    @Override
    public final Weapon getWeapon() {
        return this.weapon;
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(health);
        final int magic1 = 1231;
        final int magic2 = 1237;
        result = prime * result + (isCurrentlyJumping ? magic1 : magic2);
        result = prime * result + ((player == null) ? 0 : player.hashCode());
        result = prime * result + (queuedForDestruction ? magic1 : magic2);
        result = prime * result + (shouldShoot ? magic1 : magic2);
        result = prime * result + ((weapon == null) ? 0 : weapon.hashCode());
        return result;
    }

    @Override
    public final boolean isQueuedForDestruction() {
        return queuedForDestruction;
    }

    /**
     * Return if the player can shoot.
     * 
     * @return the shouldShoot state
     */
    public final boolean isShouldShoot() {
        return shouldShoot;
    }

    @Override
    public final void jump() {
        if (!isCurrentlyJumping) {
            player.applyLinearImpulse(new Vector2(0, MAX_JUMP), player.getWorldCenter(), true);
            isCurrentlyJumping = true;
        }
    }

    @Override
    public final void move(final Vector2 direction) {
        final Vector2 linearVelocity = player.getLinearVelocity().cpy();
        direction.scl(MAX_VELOCITY);
        linearVelocity.x = direction.x;
        player.setLinearVelocity(linearVelocity);
    }

    @Override
    public final void queueForDestruction() {
        queuedForDestruction = true;
    }

    @Override
    public final void setJumpState(final boolean isJumping) {
        this.isCurrentlyJumping = isJumping;
    }

    /**
     * Set the shooting state to the player.
     * 
     * @param shouldShoot  the shooting state
     */
    public final void setShouldShoot(final boolean shouldShoot) {
        this.shouldShoot = shouldShoot;
    }

    @Override
    public final void setWeapon(final Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public final void shoot() {
        if (weapon.canShoot() && shouldShoot) {
            weapon.shoot();
        }
        shouldShoot = false;
    }

    @Override
    public final String toString() {
        return "PlayerImpl [health=" + health + ", isJumping=" + isCurrentlyJumping + ", shouldShoot=" + shouldShoot
                + ", player=" + player + ", queuedForDestruction=" + queuedForDestruction + "]";
    }

    @Override
    public void update() {
    }
}
