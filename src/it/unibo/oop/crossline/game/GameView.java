package it.unibo.oop.crossline.game;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.actor.player.Player;
import it.unibo.oop.crossline.game.bullet.Bullet;
import it.unibo.oop.crossline.game.camera.Camera;
import it.unibo.oop.crossline.game.wave.Wave;

/**
 * This class manages the rendering of the game world the the main application viewport.
 */
public interface GameView {

    /**
     * Get the current active camera.
     * @return the camera
     */
    Camera getCamera();

    /**
     * Set the tiled map to show.
     * @param tiledMap the tiled map
     */
    void setTiledMap(TiledMap tiledMap);

    /**
     * Set the world to render.
     * @param world the world
     */
    void setWorld(World world);

    /**
     * Render the view.
     * @param delta the delta
     */
    void render(float delta);

    /**
     * Set the player to display.
     * @param player the player
     */
    void setPlayer(Player player);

    /**
     * Set the enemies to display.
     * @param wave the wave of enemies
     */
    void setEnemySkin(Wave wave);

    /**
     * Dispose of the resources.
     */
    void dispose();

    /**
     * Initialize the skin for the bullet.
     *@param entity object to take class.
    */
    void setBulletSkin(Bullet entity);

}
