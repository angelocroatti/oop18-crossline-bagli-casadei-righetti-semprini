package it.unibo.oop.crossline.game.world.platform;

import it.unibo.oop.crossline.game.attributes.Physical;

/**
 * This interface represents a platform.
 */
public interface Platform extends Physical {

}
