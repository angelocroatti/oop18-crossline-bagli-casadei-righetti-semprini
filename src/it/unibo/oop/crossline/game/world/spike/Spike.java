package it.unibo.oop.crossline.game.world.spike;

import it.unibo.oop.crossline.game.attributes.Physical;

/**
 * This interface represents a spike.
 */
public interface Spike extends Physical {

}
