package it.unibo.oop.crossline.game.world.spike;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Test class for SpikeFactoryImpl.
 */
public class SpikeFactoryImplTest {

    private static final float EARTH_GRAVITY = -9.8f;

    /**
     * Test the spikeFactory correct usage.
     */
    @Test
    public final void test() {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        final SpikeImpl spike1 = new SpikeImpl(world, new Vector2(1, 0));
        final SpikeFactoryImpl spikeFactory = new SpikeFactoryImpl(world);
        final SpikeImpl spike2 = (SpikeImpl) spikeFactory.createSpike(new Vector2(1, 0));
        assertEquals("spikeFactory has created a spike with position equals to spike1's position", spike2.getPosition(),
                spike1.getPosition());
    }

}
