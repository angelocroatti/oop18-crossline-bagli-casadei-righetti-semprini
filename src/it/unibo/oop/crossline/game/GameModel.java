package it.unibo.oop.crossline.game;

import java.util.Observer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.World;
import it.unibo.oop.crossline.game.actor.player.Player;
import it.unibo.oop.crossline.game.wave.Wave;

/**
 * This is the interface that represents the main game logic class.
 */
public interface GameModel {

    /**
     * Add an observer that is notified when the waves end.
     * @param observer the observer to add
     */
    void addObserver(Observer observer);

    /**
     * Get the physics world.
     * @return the phsyics world
     */
    World getWorld();

    /**
     * Get the world map.
     * @return the world map
     */
    TiledMap getTiledMap();

    /**
     * Get the current wave.
     * @return the current wave
     */
    Wave getCurrentWave();

    /**
     * Get the current game player.
     * @return the player
     */
    Player getPlayer();

    /**
     * Update the game world.
     */
    void update();

    /**
     * Get if the game should exit.
     * @return the value that tells if the game should exit.
     */
    boolean shouldExit();

}
