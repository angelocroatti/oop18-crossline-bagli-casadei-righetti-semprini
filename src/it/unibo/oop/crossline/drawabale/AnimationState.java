package it.unibo.oop.crossline.drawabale;

/**
 *
 * Parameter for draw animation.
 *
 */
public enum AnimationState {

    /**
     *
     */
    IS_IDLE,
    /**
    *
    */
    IS_JUMPING,
    /**
    *
    */
    IS_RUNING,
    /**
    *
    */
    IS_SHOOTING,
    /**
    *
    */
    AS_HIT,
    /**
    *
    */
    AS_POWER;

    /**
     * Set Speed of animation frame.
     */
    public static final float SPEED = 0.3f;

    /**
     * To know position of package of file.
     * @return String of path resource
     */
    public static String getAtlas() {
        return "Sprites.atlas";
    }
}


