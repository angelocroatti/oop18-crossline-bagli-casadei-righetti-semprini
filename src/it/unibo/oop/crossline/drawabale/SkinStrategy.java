package it.unibo.oop.crossline.drawabale;
import it.unibo.oop.crossline.drawabale.product.SkinProduct;
import it.unibo.oop.crossline.drawabale.product.attributes.idle.Idle;
import it.unibo.oop.crossline.drawabale.product.attributes.jump.Jump;
import it.unibo.oop.crossline.game.actor.Actor;

/**
 *
 * Strategy to decide what type of animation draw.
 *
 * @param <Y> ayy
 */
public class SkinStrategy<Y extends Actor> {


        /**
         * Interface for every type of {@link Actor}.
         *
         */
        private final Idle idleAnimation;
        private final Jump jumpAnimation;
        private SkinProduct entity;

        /**
         * Skin would now who is, player or robot.
         *
         * @param idle Idle concrete class
         * @param jump Jump concrete class
         */
        public SkinStrategy(final Idle idle, final Jump jump) {
            super();
            this.idleAnimation = idle;
            this.jumpAnimation = jump;
        }

        /**
         * this method will call every render time to set animationEntity with correct frame
         * that will draw in the view.
         * @return {@link it.unibo.oop.crossline.drawabale.product.SkinProductAnimatedImpl} animation in idle state
         */
        public SkinProduct idle() {
            entity = this.idleAnimation.idle();
            return entity;
        }

        /**
         * this method will call every time you would to set {@link it.unibo.oop.crossline.drawabale.product.SkinProductAnimatedImpl} with correct animation
         * that will draw in the view.
         * @return {@link it.unibo.oop.crossline.drawabale.product.SkinProductAnimatedImpl} animation in jump state
         */
        public SkinProduct jump() {
            entity = this.jumpAnimation.jump();
            return entity;
        }


}
