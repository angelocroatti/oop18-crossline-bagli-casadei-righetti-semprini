package it.unibo.oop.crossline.drawabale.factory;

import java.util.Optional;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

import it.unibo.oop.crossline.drawabale.AnimationState;
import it.unibo.oop.crossline.drawabale.product.SkinProduct;
import it.unibo.oop.crossline.drawabale.product.SkinProductAnimatedImpl;
import it.unibo.oop.crossline.game.bullet.Bullet;

/**
 * Create skin entity for draw in world {@link Object}.
 * @param <X> class to draw.
 *
 */
public class SkinFactoryObjectImpl<X> implements SkinFactory<X> {



    private Optional<SkinProduct> animation;

    /**
     * @return SkinProduct that may be null.
     */
    @Override
    public SkinProduct createSkin(final X entity) {

        if (entity instanceof Bullet) {
          this.animation = Optional.of(new SkinProductAnimatedImpl("Sprites.atlas",
                  "bullet", AnimationState.SPEED, PlayMode.LOOP));
          animation.get().setState(AnimationState.IS_IDLE); 

      } else {
          System.err.println("Is not possible to draw this entity" + entity.toString());
      }

      if (this.animation.isPresent()) {

          return this.animation.get();
      }
        return null;

    }

    @Override
    public final SkinProduct createSkinWithState(final X entity, final AnimationState a) {


        if (entity instanceof Bullet) {
            this.animation = Optional.of(new SkinProductAnimatedImpl("/Sprites.atlas",
                    "idle", AnimationState.SPEED, PlayMode.LOOP));
            animation.get().setState(a); 

        } else {
            System.err.println("Is not possible to draw this entity" + entity.toString());
        }

        if (this.animation.isPresent()) {

            return this.animation.get();
        }

        switch (this.animation.get().getState()) {
        case IS_IDLE:

            break;

        case AS_HIT:

            break;

        case AS_POWER:

            break;

        default:
            break;
        }
        return null;
    }


}
