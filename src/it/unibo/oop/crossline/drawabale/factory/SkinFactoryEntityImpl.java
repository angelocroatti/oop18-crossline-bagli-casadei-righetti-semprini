package it.unibo.oop.crossline.drawabale.factory;

import it.unibo.oop.crossline.drawabale.AnimationState;
import it.unibo.oop.crossline.drawabale.SkinStrategy;
import it.unibo.oop.crossline.drawabale.product.SkinProduct;
import it.unibo.oop.crossline.drawabale.product.attributes.idle.IdlePlayerImpl;
import it.unibo.oop.crossline.drawabale.product.attributes.idle.IdleRobotImpl;
import it.unibo.oop.crossline.drawabale.product.attributes.jump.JumpPlayerImpl;
import it.unibo.oop.crossline.drawabale.product.attributes.jump.JumpRobotImpl;
import it.unibo.oop.crossline.game.actor.Actor;
import it.unibo.oop.crossline.game.actor.player.Player;
import it.unibo.oop.crossline.game.actor.robot.Robot;

/**
 * Create skin entity for draw in world {@link Actor}.
 * @param <X> type of skin to draw
 */
public class SkinFactoryEntityImpl<X extends Actor> implements SkinFactory<X> {

    private SkinProduct animated;
    private SkinStrategy<X> skinStrategy;

    @Override
    public final SkinProduct createSkin(final X entity) {


        /*
         * Determine what skin would be constructed by class reference.
         */
        if (entity instanceof Player) {


            this.skinStrategy = new SkinStrategy<X>(new IdlePlayerImpl(),
                    new JumpPlayerImpl());
            this.animated = skinStrategy.idle();

        } else if (entity instanceof Robot) {


            this.skinStrategy = new SkinStrategy<X>(new IdleRobotImpl(),
                  new JumpRobotImpl());
            this.animated = skinStrategy.idle();

        } else {
            System.err.println("Is not possible to draw this entity" + entity.toString());
        }
        return this.animated;
    }

    @Override
    public final SkinProduct createSkinWithState(final X entity, final AnimationState a) {

        if (entity instanceof Player) {


            this.skinStrategy = new SkinStrategy<X>(new IdlePlayerImpl(),
                    new JumpPlayerImpl());

        } else if (entity instanceof Robot) {


            this.skinStrategy = new SkinStrategy<X>(new IdleRobotImpl(),
                  new JumpRobotImpl());

        } else {
            System.err.println("Is not possible to draw this entity" + entity.toString());
        }

        switch (a) {
        case IS_IDLE:

           this.animated = this.skinStrategy.idle();
            break;

        case IS_JUMPING:

            this.animated = this.skinStrategy.jump();
            break;

        default:
            break;


    }
        return this.animated;

    }

}
