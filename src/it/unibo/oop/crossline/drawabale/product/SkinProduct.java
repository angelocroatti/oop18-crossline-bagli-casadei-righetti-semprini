package it.unibo.oop.crossline.drawabale.product;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import it.unibo.oop.crossline.drawabale.AnimationState;

/**
 * Skin of object of the world.
 *
 */
public interface SkinProduct {

    /**
     * To call before batch.begin().
     *
     */
    void swichFrameByTime();

    /**
   *
   *   to call between batch.begin() and batch.end().
   *
   * @param batch place where draw
   * @param pos is vector where draw animation
   */
    void drawOnSpriteBatch(SpriteBatch batch, Vector2 pos);

    /**
     * Let resource to be free.
     */
     void dispose();

     /**
      * know the type of skin.
      * @return {@link AnimationState} of skin
      */
    AnimationState getState();

    /**
     * set the type of skin.
     * @param a animation type.
     */
    void setState(AnimationState a);
}
