package it.unibo.oop.crossline.drawabale.product.attributes.jump;

import it.unibo.oop.crossline.drawabale.product.SkinProduct;


/**
 *
 * Jump contract.
 *
 */
public interface Jump {

    /**
    *
    * create Jump method.
    * @return {@link it.unibo.oop.crossline.drawabale.product.SkinProduct} Animation that jump
    */
    SkinProduct jump();
}
